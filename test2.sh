#!/bin/bash
set -e
echo '----------ENVS----------'
env
echo '----------SYS----------'
free -h
cat /proc/cpuinfo
df -h
echo '----------START---------'
export HOME=`pwd`

PREFIX=$HOME/deps
export PKG_CONFIG='pkg-config --static'
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/
export LD_LIBRARY_PATH=$PREFIX/lib/

cd $HOME
curl -L https://www.zlib.net/zlib-1.2.11.tar.gz | tar xz
cd zlib-*/
./configure --prefix=$PREFIX --static
make -j`nproc`
make install

cd $HOME
curl -L https://tukaani.org/xz/xz-5.2.5.tar.gz | tar xz
cd xz-*/
./configure --prefix=$PREFIX --disable-xz --disable-lzma-links --disable-scripts --disable-doc --enable-static --disable-shared --disable-nls
make -j`nproc`
make install

cd $HOME
curl -L https://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.16.tar.gz | tar xz
cd libiconv-*/
./configure --prefix=$PREFIX --enable-static --disable-shared
make -j`nproc`
make install

cd $HOME
curl -L ftp://xmlsoft.org/libxml2/libxml2-2.9.10.tar.gz | tar xz
cd libxml2-*/
LDFLAGS="$LDFLAGS -L$PREFIX/lib" CFLAGS="$CFLAGS -I$PREFIX/include" ./configure --prefix=$PREFIX --disable-shared --enable-static --without-debug --without-ftp --without-python
make -j`nproc`
make install

cd $HOME
curl -L https://www.nasm.us/pub/nasm/releasebuilds/2.15.05/nasm-2.15.05.tar.gz | tar xz
cd nasm-*/
./configure --prefix=$HOME/tools
make -j`nproc`
make install
export PATH=$PATH:$HOME/tools/bin

cd $HOME
curl -L https://wimlib.net/downloads/wimlib-1.13.3.tar.gz | tar xz
cd wimlib-*/
LDFLAGS="$LDFLAGS -L$PREFIX/lib" CFLAGS="$CFLAGS -I$PREFIX/include" ./configure --prefix=$HOME/wimlib --disable-shared --enable-static --enable-ssse3-sha1 --without-ntfs-3g --without-fuse
make -j`nproc`
strip -s -x wimlib-imagex
mv wimlib-imagex $HOME/

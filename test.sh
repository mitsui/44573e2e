#!/bin/bash
echo '----------ENVS----------'
env
echo '----------SYS----------'
free -h
cat /proc/cpuinfo
df -h
echo '----------START---------'
export HOME=`pwd`

curl -LO https://sourceforge.net/projects/lazarus/files/Lazarus%20Linux%20i386%20DEB/Lazarus%202.0.10/fpc-src_3.2.0-1_i386.deb
curl -LO https://sourceforge.net/projects/lazarus/files/Lazarus%20Linux%20i386%20DEB/Lazarus%202.0.10/fpc-laz_3.2.0-1_i386.deb
curl -LO https://sourceforge.net/projects/lazarus/files/Lazarus%20Linux%20i386%20DEB/Lazarus%202.0.10/lazarus-project_2.0.10-0_i386.deb
dpkg -i fpc-laz_*.deb
dpkg -i fpc-src_*.deb
dpkg -i lazarus-project_*.deb
apt-get update
apt-get install --no-install-recommends -y -f
#cd /usr/share/fpcsrc/3.0.4
#make clean all OS_TARGET=win32 CPU_TARGET=i386
#make crossinstall OS_TARGET=win32 CPU_TARGET=i386 INSTALL_PREFIX=/usr
#ln -sf /usr/lib/fpc/3.0.4/ppcross386 /usr/bin/ppcross386
git clone https://github.com/lunafe/pray.git $HOME/Pray
cd $HOME/Pray
/usr/bin/lazbuild --cpu=i386 --bm=Release Pray.lpi
ls -hl
